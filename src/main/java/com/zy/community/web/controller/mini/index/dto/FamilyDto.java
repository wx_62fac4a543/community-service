package com.zy.community.web.controller.mini.index.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 家庭成员
 * 代表 业主、亲属、其他、租户
 */
public class FamilyDto implements Serializable {

    private String buildingName;

    private String unitName;

    private String roomName;

    private List<MemberDto> members = new ArrayList<>();//家庭成员集合

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long roomId;



    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public List<MemberDto> getMembers() {
        return members;
    }

    public void setMembers(List<MemberDto> members) {
        this.members = members;
    }

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }


}
