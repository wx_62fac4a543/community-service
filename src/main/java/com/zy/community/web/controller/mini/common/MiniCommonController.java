package com.zy.community.web.controller.mini.common;

import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.mini.service.common.MiniCommonService;
import com.zy.community.web.controller.mini.common.dto.DictDto;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * 通用处理
 */
@RestController
@RequestMapping("/mini/community/common")
public class MiniCommonController {
    @Resource
    private MiniCommonService miniCommonService;

    @PostMapping("/upload")
    public ZyResult<String> commonUpload(MultipartFile file) {
        return miniCommonService.uploadFile(file);
    }


    @GetMapping("/dict/{type}")
    public ZyResult<List<DictDto>> findDictByType(@PathVariable("type") String type){
        return miniCommonService.selectDictByType(type);
    }
    @GetMapping("/existsBind/{communityId}")
    public ZyResult<Boolean> findCurrentUserHasBindInfo(@PathVariable("communityId") Long communityId) {
        return miniCommonService.findCurrentUserHasBindInfo(communityId);
    }
}
