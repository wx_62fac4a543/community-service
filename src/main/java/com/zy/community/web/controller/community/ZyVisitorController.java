package com.zy.community.web.controller.community;

import java.util.List;

import com.zy.community.common.annotation.Log;
import com.zy.community.common.core.controller.BaseController;
import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.common.core.page.TableDataInfo;
import com.zy.community.common.enums.BusinessType;
import com.zy.community.common.utils.poi.ExcelUtil;
import com.zy.community.community.domain.ZyVisitor;
import com.zy.community.community.domain.dto.ZyVisitorDto;
import com.zy.community.community.service.IZyVisitorService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 访客邀请 Controller
 *
 * @author yangdi
 * @date 2020-12-18
 */
@RestController
@RequestMapping("/system/visitor")
public class ZyVisitorController extends BaseController {
    @Resource
    private IZyVisitorService zyVisitorService;

    /**
     * 查询访客邀请 列表
     */
    @PreAuthorize("@ss.hasPermi('system:visitor:list')")
    @GetMapping("/list")
    public TableDataInfo list(ZyVisitor zyVisitor) {
        startPage();
        List<ZyVisitorDto> list = zyVisitorService.selectZyVisitorList(zyVisitor);
        return getDataTable(list);
    }

    /**
     * 导出访客邀请 列表
     */
    @PreAuthorize("@ss.hasPermi('system:visitor:export')")
    @Log(title = "访客邀请 ", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public ZyResult export(ZyVisitor zyVisitor) {
        List<ZyVisitorDto> list = zyVisitorService.selectZyVisitorList(zyVisitor);
        ExcelUtil<ZyVisitorDto> util = new ExcelUtil<ZyVisitorDto>(ZyVisitorDto.class);
        return util.exportExcel(list, "visitor");
    }
}
