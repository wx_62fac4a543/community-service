package com.zy.community.web.controller.system;

import com.zy.community.common.annotation.Log;
import com.zy.community.common.core.controller.BaseController;
import com.zy.community.common.core.domain.entity.SysUser;
import com.zy.community.common.core.domain.model.LoginUser;
import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.common.core.domain.support.Kv;
import com.zy.community.common.enums.BusinessType;
import com.zy.community.common.utils.SecurityUtils;
import com.zy.community.common.utils.ServletUtils;
import com.zy.community.framework.web.service.TokenService;
import com.zy.community.mini.service.common.MiniCommonService;
import com.zy.community.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * 个人信息 业务处理
 * 
 * @author yangdi
 */
@RestController
@RequestMapping("/system/user/profile")
public class SysProfileController extends BaseController
{
    @Autowired
    private ISysUserService userService;

    @Autowired
    private TokenService tokenService;
    @Resource
    private MiniCommonService miniCommonService;

    /**
     * 个人信息
     */
    @GetMapping
    public Kv profile()
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        SysUser user = loginUser.getUser();
        Kv ajax = Kv.create().set("code",200).set("msg","操作成功").set("data",user);
        ajax.put("roleGroup", userService.selectUserRoleGroup(loginUser.getUsername()));
        ajax.put("postGroup", userService.selectUserPostGroup(loginUser.getUsername()));
        return ajax;
    }

    /**
     * 修改用户
     */
    @Log(title = "个人信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public ZyResult updateProfile(@RequestBody SysUser user)
    {
        if (userService.updateUserProfile(user) > 0)
        {
            LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
            // 更新缓存用户信息
            loginUser.getUser().setNickName(user.getNickName());
            loginUser.getUser().setPhonenumber(user.getPhonenumber());
            loginUser.getUser().setEmail(user.getEmail());
            loginUser.getUser().setSex(user.getSex());
            tokenService.setLoginUser(loginUser);
            return ZyResult.success("修改成功");
        }
        return ZyResult.fail("修改个人信息异常，请联系管理员");
    }

    /**
     * 重置密码
     */
    @Log(title = "个人信息", businessType = BusinessType.UPDATE)
    @PutMapping("/updatePwd")
    public ZyResult updatePwd(String oldPassword, String newPassword)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String userName = loginUser.getUsername();
        String password = loginUser.getPassword();
        if (!SecurityUtils.matchesPassword(oldPassword, password))
        {
            return ZyResult.fail("修改密码失败，旧密码错误");
        }
        if (SecurityUtils.matchesPassword(newPassword, password))
        {
            return ZyResult.fail("新密码不能与旧密码相同");
        }
        if (userService.resetUserPwd(userName, SecurityUtils.encryptPassword(newPassword)) > 0)
        {
            // 更新缓存用户密码
            loginUser.getUser().setPassword(SecurityUtils.encryptPassword(newPassword));
            tokenService.setLoginUser(loginUser);
            return ZyResult.success("修改成功");
        }
        return ZyResult.fail("修改密码异常，请联系管理员");
    }

    /**
     * 头像上传
     */
    @Log(title = "用户头像", businessType = BusinessType.UPDATE)
    @PostMapping("/avatar")
    public Kv avatar(@RequestParam("avatarfile") MultipartFile file) throws IOException
    {
        String avatar = "";
        if (!file.isEmpty())
        {
            LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
            ZyResult<String> zyResult = miniCommonService.uploadFile(file);
            if(null != zyResult && null != zyResult.getData()){
                avatar = zyResult.getData();
            }
            if (userService.updateUserAvatar(loginUser.getUsername(), avatar))
            {
                Kv ajax = Kv.create().set("code",200).set("msg","操作成功");
                ajax.put("imgUrl", avatar);
                // 更新缓存用户头像
                loginUser.getUser().setAvatar(avatar);
                tokenService.setLoginUser(loginUser);
                return ajax;
            }
        }
        return Kv.create().set("code",500).set("msg","上传图片异常，请联系管理员");
    }
}
