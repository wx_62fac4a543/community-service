package com.zy.community.web.controller.mini.index;

import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.mini.service.index.MiniMyHouseService;
import com.zy.community.web.controller.mini.life.dto.BindResultDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/mini/community/house")
public class MiniMyHouseController {
    @Resource
    private MiniMyHouseService miniMyHouseService;

    /**
     * 查询绑定信息
     * @param communityId 小区Id
     * @return 绑定结果
     */
    @GetMapping("/info/{communityId}")
    public ZyResult<BindResultDto> findOwnerBindResult(@PathVariable("communityId") Long communityId){
        return miniMyHouseService.findOwnerBindResult(communityId);
    }
}
