package com.zy.community.web.controller.mini.index;

import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.mini.service.index.MiniCommunityService;
import com.zy.community.web.controller.mini.index.dto.CommunityDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 小区接口
 */
@RestController
@RequestMapping("/mini/community/community")
public class MiniCommunityController {
    @Resource
    private MiniCommunityService miniCommunityService;

    @GetMapping("/all")
    public ZyResult<List<CommunityDto>> findAllCommunity() {
        return miniCommunityService.findAllCommunity();
    }
}
