package com.zy.community.web.controller.community;

import com.zy.community.common.annotation.Log;
import com.zy.community.common.core.controller.BaseController;
import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.common.core.page.TableDataInfo;
import com.zy.community.common.enums.BusinessType;
import com.zy.community.community.domain.ZyOwner;
import com.zy.community.community.domain.dto.ZyOwnerDto;
import com.zy.community.community.service.IZyOwnerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 业主 Controller
 *
 * @author yangdi
 * @date 2020-12-16
 */
@Api(tags = "业主")
@RestController
@RequestMapping("/system/owner")
public class ZyOwnerController extends BaseController {
    @Resource
    private IZyOwnerService zyOwnerService;

    /**
     * 查询业主 列表
     */
    @ApiOperation(value = "查询业主")
    @PreAuthorize("@ss.hasPermi('system:owner:list')")
    @GetMapping("/list")
    public TableDataInfo list(ZyOwner zyOwner) {
        startPage();
        List<ZyOwnerDto> list = zyOwnerService.selectZyOwnerList(zyOwner);
        return getDataTable(list);
    }

    /**
     * 解绑
     */
    @ApiOperation(value = "解绑")
    @PreAuthorize("@ss.hasPermi('system:owner:remove')")
    @Log(title = "解绑 ", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ownerRoomId}")
    public ZyResult remove(@PathVariable Long ownerRoomId) {
        return toAjax(zyOwnerService.deleteZyOwnerByIds(ownerRoomId));
    }
}
