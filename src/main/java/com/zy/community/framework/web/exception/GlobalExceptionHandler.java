package com.zy.community.framework.web.exception;

import com.zy.community.common.constant.HttpStatus;
import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.common.exception.BaseException;
import com.zy.community.common.exception.CustomException;
import com.zy.community.common.exception.DemoModeException;
import com.zy.community.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * 全局异常处理器
 * 
 * @author yangdi
 */
@RestControllerAdvice
public class GlobalExceptionHandler
{
    private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    /**
     * 基础异常
     */
    @ExceptionHandler(BaseException.class)
    public ZyResult baseException(BaseException e)
    {
        return ZyResult.fail(e.getMessage());
    }

    /**
     * 业务异常
     */
    @ExceptionHandler(CustomException.class)
    public ZyResult businessException(CustomException e)
    {
        if (StringUtils.isNull(e.getCode()))
        {
            return ZyResult.fail(e.getMessage());
        }
        return ZyResult.fail(e.getCode(), e.getMessage());
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    public ZyResult handlerNoFoundException(Exception e)
    {
        log.error(e.getMessage(), e);
        return ZyResult.fail(HttpStatus.NOT_FOUND, "路径不存在，请检查路径是否正确");
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ZyResult handleAuthorizationException(AccessDeniedException e)
    {
        log.error(e.getMessage());
        return ZyResult.fail(HttpStatus.FORBIDDEN, "没有权限，请联系管理员授权");
    }

    @ExceptionHandler(AccountExpiredException.class)
    public ZyResult handleAccountExpiredException(AccountExpiredException e)
    {
        log.error(e.getMessage(), e);
        return ZyResult.fail(e.getMessage());
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    public ZyResult handleUsernameNotFoundException(UsernameNotFoundException e)
    {
        log.error(e.getMessage(), e);
        return ZyResult.fail(e.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public ZyResult handleException(Exception e)
    {
        log.error(e.getMessage(), e);
        return ZyResult.fail(e.getMessage());
    }

    /**
     * 自定义验证异常
     */
    @ExceptionHandler(BindException.class)
    public ZyResult validatedBindException(BindException e)
    {
        log.error(e.getMessage(), e);
        String message = e.getAllErrors().get(0).getDefaultMessage();
        return ZyResult.fail(message);
    }

    /**
     * 自定义验证异常
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Object validExceptionHandler(MethodArgumentNotValidException e)
    {
        log.error(e.getMessage(), e);
        String message = e.getBindingResult().getFieldError().getDefaultMessage();
        return ZyResult.fail(message);
    }

    /**
     * 演示模式异常
     */
    @ExceptionHandler(DemoModeException.class)
    public ZyResult demoModeException(DemoModeException e)
    {
        return ZyResult.fail("演示模式，不允许操作");
    }
}
