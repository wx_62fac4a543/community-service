package com.zy.community.community.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zy.community.common.constant.HttpStatus;
import com.zy.community.common.core.domain.ManageParametersUtils;
import com.zy.community.common.exception.CustomException;
import com.zy.community.common.utils.RequestHeaderUtils;
import com.zy.community.common.utils.SecurityUtils;
import com.zy.community.common.utils.StringUtils;
import com.zy.community.community.domain.ZyOwnerRoom;
import com.zy.community.community.domain.ZyOwnerRoomRecord;
import com.zy.community.community.domain.dto.ZyOwnerRoomDto;
import com.zy.community.community.domain.dto.ZyOwnerRoomRecordDto;
import com.zy.community.community.domain.vo.AuditType;
import com.zy.community.community.domain.vo.RoomStatus;
import com.zy.community.community.mapper.ZyOwnerRoomMapper;
import com.zy.community.community.mapper.ZyOwnerRoomRecordMapper;
import com.zy.community.community.service.IZyOwnerRoomService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * 房屋绑定 Service业务层处理
 *
 * @author yangdi
 * @date 2020-12-15
 */
@Service
public class ZyOwnerRoomServiceImpl implements IZyOwnerRoomService {
    @Resource
    private ZyOwnerRoomMapper zyOwnerRoomMapper;

    @Resource
    private ZyOwnerRoomRecordMapper zyOwnerRoomRecordMapper;

    @Value("${community.value}")
    private String propertyId;

    /**
     * 查询房屋绑定 列表
     *
     * @param zyOwnerRoom 房屋绑定
     * @return 房屋绑定
     */
    @Override
    public List<ZyOwnerRoomDto> selectZyOwnerRoomList(ZyOwnerRoom zyOwnerRoom) {
        zyOwnerRoom.setOwnerType("yz");
        zyOwnerRoom.setCommunityId(RequestHeaderUtils.GetCommunityId(propertyId));
        return zyOwnerRoomMapper.selectZyOwnerRoomList(zyOwnerRoom);
    }

    /**
     * 修改房屋绑定
     *
     * @param zyOwnerRoomRecord 房屋绑定
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int updateZyOwnerRoom(ZyOwnerRoomRecord zyOwnerRoomRecord) {
        if (RoomStatus.Reject.equals(zyOwnerRoomRecord.getRoomStatus())&& StringUtils.isEmpty(zyOwnerRoomRecord.getRecordAuditOpinion())){
            throw new CustomException("请输入拒绝原因", HttpStatus.BAD_REQUEST);
        }

        ZyOwnerRoom zyOwnerRoom = zyOwnerRoomMapper.selectById(zyOwnerRoomRecord.getOwnerRoomId());
        List<ZyOwnerRoom> zyOwnerRooms = zyOwnerRoomMapper.selectList(new QueryWrapper<>(new ZyOwnerRoom() {
            {
                setRoomId(zyOwnerRoom.getRoomId());
                setRoomStatus(RoomStatus.Binding);
            }
        }));
        if (RoomStatus.Binding.equals(zyOwnerRoomRecord.getRoomStatus())&& zyOwnerRooms.size()>0){
            throw new CustomException("该房屋以绑定业主", HttpStatus.CONFLICT);
        }

        zyOwnerRoom.setRoomStatus(zyOwnerRoomRecord.getRoomStatus());
        int i = zyOwnerRoomMapper.updateById(ManageParametersUtils.updateMethod(zyOwnerRoom));
        if (i == 0){
            throw new CustomException("绑定表状态为被修改", HttpStatus.NOT_MODIFIED);
        }
        zyOwnerRoomRecord.setCommunityId(zyOwnerRoom.getCommunityId());
        zyOwnerRoomRecord.setBuildingId(zyOwnerRoom.getBuildingId());
        zyOwnerRoomRecord.setUnitId(zyOwnerRoom.getUnitId());
        zyOwnerRoomRecord.setRoomId(zyOwnerRoom.getRoomId());
        zyOwnerRoomRecord.setOwnerId(zyOwnerRoom.getOwnerId());
        zyOwnerRoomRecord.setOwnerType(zyOwnerRoom.getOwnerType());
        zyOwnerRoomRecord.setRecordAuditType(AuditType.Web);
        zyOwnerRoomRecord.setCreateById(SecurityUtils.getLoginUser().getUser().getUserId());
        return zyOwnerRoomRecordMapper.insert(ManageParametersUtils.saveMethod(zyOwnerRoomRecord));
    }

    @Override
    public List<ZyOwnerRoomRecordDto> queryOwnerRoomRecordByOwnerRoomId(Long ownerRoomId) {
        return zyOwnerRoomRecordMapper.queryOwnerRoomRecordByOwnerRoomId(ownerRoomId);
    }
}
