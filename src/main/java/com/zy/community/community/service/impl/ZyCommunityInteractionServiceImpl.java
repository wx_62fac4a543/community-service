package com.zy.community.community.service.impl;
import com.zy.community.common.utils.DateUtils;
import com.zy.community.community.domain.ZyCommunityInteraction;
import com.zy.community.community.domain.ZyFiles;
import com.zy.community.community.domain.dto.ZyCommentDto;
import com.zy.community.community.domain.dto.ZyCommunityInteractionDto;
import com.zy.community.community.mapper.ZyCommunityInteractionMapper;
import com.zy.community.community.mapper.ZyOwnerMapper;
import com.zy.community.community.service.IZyCommentService;
import com.zy.community.community.service.IZyFilesService;
import com.zy.community.framework.security.mini.MiniContextUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 社区互动Service业务层处理
 * 
 * @author yin
 * @date 2020-12-17
 */
@Service
public class ZyCommunityInteractionServiceImpl
{
    @Resource
    private ZyCommunityInteractionMapper zyCommunityInteractionMapper;
    @Resource
    private IZyFilesService zyFilesService;
    @Resource
    private ZyOwnerMapper zyOwnerMapper;
    @Resource
    private IZyCommentService zyCommentService;

    /**
     * 查询社区互动
     * 
     * @param interactionId 社区互动ID
     * @return 社区互动
     */
    public ZyCommunityInteractionDto selectZyCommunityInteractionById(Long interactionId)
    {
        ZyCommentDto zyComment = new ZyCommentDto();
        zyComment.setInteractionId(interactionId);
        List<ZyCommentDto> zyComments = zyCommentService.selectZyCommentList(zyComment);
        //ZyCommunityInteractionDto dto = new ZyCommunityInteractionDto();
        ZyCommunityInteractionDto dto = zyCommunityInteractionMapper.selectInteractionById(interactionId);
       // BeanUtils.copyBeanProp(dto,zyCommunityInteraction);
        dto.setZyCommentList(zyComments);
        return dto;
    }

    /**
     * 查询社区互动列表
     * 
     * @param zyCommunityInteraction 社区互动
     * @return 社区互动
     */
    public List<ZyCommunityInteractionDto> selectZyCommunityInteractionList(ZyCommunityInteractionDto zyCommunityInteraction)
    {
        List<ZyCommunityInteractionDto> interactionDtos = zyCommunityInteractionMapper.selectInteractionList(zyCommunityInteraction);
        for (ZyCommunityInteractionDto communityInteraction : interactionDtos) {
            Long parentId = communityInteraction.getInteractionId();
            List wyglFilesList = zyFilesService.getWyglFilesList(parentId);
            communityInteraction.setUrlList(wyglFilesList);
        }
        return interactionDtos;
    }

    /**
     * 新增社区互动
     * 
     * @param zyCommunityInteraction 社区互动
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertZyCommunityInteraction(ZyCommunityInteractionDto zyCommunityInteraction)
    {
        int i = 0;
        ZyCommunityInteraction interaction = new ZyCommunityInteraction();
        interaction.setCreateTime(DateUtils.getNowDate());
        //interaction.setCreateBy(MiniContextUtils.getPhoneNum());
        Long ownerId = zyOwnerMapper.findOwnerIdByOpenId(MiniContextUtils.getOpenId());
        if(null != ownerId){
            interaction.setUserId(ownerId);
            interaction.setContent(zyCommunityInteraction.getContent());
            i = zyCommunityInteractionMapper.insert(interaction);
            ZyFiles[] zyFiles = zyCommunityInteraction.getZyFiles();
            for (ZyFiles zyFile : zyFiles) {
                zyFile.setParentId(interaction.getInteractionId());
            }
            zyFilesService.insertZyFiles(zyFiles);
        }
        return i;
    }

    /**
     * 修改社区互动
     * 
     * @param zyCommunityInteraction 社区互动
     * @return 结果
     */
    public int updateZyCommunityInteraction(ZyCommunityInteraction zyCommunityInteraction)
    {
        zyCommunityInteraction.setUpdateTime(DateUtils.getNowDate());
        zyCommunityInteraction.setUpdateBy(MiniContextUtils.getPhoneNum());
        return zyCommunityInteractionMapper.updateById(zyCommunityInteraction);
    }

    /**
     * 批量删除社区互动
     * 
     * @param interactionIds 需要删除的社区互动ID
     * @return 结果
     */
    public int deleteZyCommunityInteractionByIds(Long[] interactionIds)
    {
        return zyCommunityInteractionMapper.deleteById(interactionIds);
    }

    /**
     * 删除社区互动信息
     * 
     * @param zyCommunityInteraction 社区互动ID
     * @return 结果
     */
    public int deleteZyCommunityInteractionById(ZyCommunityInteraction zyCommunityInteraction)
    {
        zyCommunityInteraction.setDelFlag(1);//删除状态
        return zyCommunityInteractionMapper.updateById(zyCommunityInteraction);
    }
}
