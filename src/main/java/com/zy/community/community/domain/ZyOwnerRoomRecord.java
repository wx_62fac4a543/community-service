package com.zy.community.community.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zy.community.common.annotation.Excel;
import com.zy.community.common.core.domain.BaseEntity;
import com.zy.community.community.domain.vo.AuditType;
import com.zy.community.community.domain.vo.RoomStatus;

/**
 * 房屋绑定记录 对象 zy_owner_room_record
 *
 * @author yangdi
 * @date 2020-12-15
 */
public class ZyOwnerRoomRecord extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 房屋绑定记录id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @TableId
    private Long recordId;

    /**
     * 房屋绑定id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long ownerRoomId;

    /**
     * 小区id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long communityId;

    /**
     * 楼栋id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long buildingId;

    /**
     * 单元id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long unitId;

    /**
     * 房间id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long roomId;

    /**
     * 业主id
     */
    @Excel(name = "业主id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long ownerId;

    /**
     * 业主类型
     */
    @Excel(name = "业主类型")
    private String ownerType;

    /**
     * 绑定状态
     */
    @Excel(name = "绑定状态")
    private RoomStatus roomStatus;

    /**
     * 审核意见
     */
    @Excel(name = "审核意见")
    private String recordAuditOpinion;

    /**
     * 审核人类型
     */
    @Excel(name = "审核人类型")
    private AuditType recordAuditType;

    /**
     * 创建人id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @Excel(name = "创建人id")
    private Long createById;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public Long getOwnerRoomId() {
        return ownerRoomId;
    }

    public void setOwnerRoomId(Long ownerRoomId) {
        this.ownerRoomId = ownerRoomId;
    }

    public Long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    public Long getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(Long buildingId) {
        this.buildingId = buildingId;
    }

    public Long getUnitId() {
        return unitId;
    }

    public void setUnitId(Long unitId) {
        this.unitId = unitId;
    }

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    public Long getCreateById() {
        return createById;
    }

    public RoomStatus getRoomStatus() {
        return roomStatus;
    }

    public void setRoomStatus(RoomStatus roomStatus) {
        this.roomStatus = roomStatus;
    }

    public void setRecordAuditOpinion(String recordAuditOpinion) {
        this.recordAuditOpinion = recordAuditOpinion;
    }

    public String getRecordAuditOpinion() {
        return recordAuditOpinion;
    }

    public AuditType getRecordAuditType() {
        return recordAuditType;
    }

    public void setRecordAuditType(AuditType recordAuditType) {
        this.recordAuditType = recordAuditType;
    }

    public void setCreateById(Long createById) {
        this.createById = createById;
    }

    @Override
    public String toString() {
        return "ZyOwnerRoomRecord{" +
                "recordId=" + recordId +
                ", ownerRoomId=" + ownerRoomId +
                ", communityId=" + communityId +
                ", buildingId=" + buildingId +
                ", unitId=" + unitId +
                ", roomId=" + roomId +
                ", ownerId=" + ownerId +
                ", ownerType='" + ownerType + '\'' +
                ", roomStatus='" + roomStatus + '\'' +
                ", recordAuditOpinion='" + recordAuditOpinion + '\'' +
                ", recordAuditType='" + recordAuditType + '\'' +
                ", createById=" + createById +
                '}';
    }
}
