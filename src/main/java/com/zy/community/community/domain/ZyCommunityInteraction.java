package com.zy.community.community.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zy.community.common.annotation.Excel;
import com.zy.community.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Arrays;

/**
 * 社区互动对象 zy_community_interaction
 * 
 * @author yin
 * @date 2020-12-17
 */
public class ZyCommunityInteraction extends BaseEntity
{

    /** id */
    @TableId
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long interactionId;

    /** 小区ID */
    @Excel(name = "小区ID")
    private Long communityId;

    /** 内容 */
    @Excel(name = "内容")
    private String content;

    /** 删除状态0默认1删除 */
    private Integer delFlag;

    /**创建人ID*/
    private Long userId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setInteractionId(Long interactionId)
    {
        this.interactionId = interactionId;
    }

    public Long getInteractionId() 
    {
        return interactionId;
    }
    public void setCommunityId(Long communityId) 
    {
        this.communityId = communityId;
    }

    public Long getCommunityId() 
    {
        return communityId;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setDelFlag(Integer delFlag) 
    {
        this.delFlag = delFlag;
    }

    public Integer getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return "ZyCommunityInteraction{" +
                "interactionId=" + interactionId +
                ", communityId=" + communityId +
                ", content='" + content + '\'' +
                ", delFlag=" + delFlag +
                ", userId=" + userId +
                '}';
    }
}
