package com.zy.community.community.domain.vo;

/**
 * 性别枚举类
 */
public enum Gender {
    /**
     * 未知
     */
    UnKnown,
    /**
     * 男
     */
    Male,
    /**
     * 女
     */
    Female,
}
