package com.zy.community.community.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zy.community.community.domain.ZyUnit;
import com.zy.community.community.domain.dto.ZyUnitDto;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 单元 Mapper接口
 *
 * @author ruoyi
 * @date 2020-12-11
 */
public interface ZyUnitMapper extends BaseMapper<ZyUnit> {

    /**
     * 查询列表
     * @param zyUnit
     * @return
     */
    @Select("<script>" +
            "SELECT " +
            "d.unit_id,d.community_id,d.building_id," +
            "d.unit_name,d.unit_code,d.unit_level," +
            "d.unit_acreage,d.unit_have_elevator,d.create_by," +
            "d.create_time,d.update_by,d.update_time," +
            "d.remark ,a.community_name as communityName,b.building_name as buildingName " +
            "FROM zy_unit d " +
            "LEFT JOIN zy_community a on a.community_id = d.community_id " +
            "LEFT JOIN zy_building b on b.building_id = d.building_id" +
            "<where>" +
            "<if test=\"unitName !=null and unitName != ''\">" +
            "and d.unit_name like concat('%',#{unitName},'%') " +
            "</if>" +
            "<if test=\"buildingId !=null and buildingId != ''\">" +
            "and d.building_id = #{buildingId} " +
            "</if>" +
            "and d.community_id = #{communityId}" +
            "${params.dataScope}" +
            "</where>" +
            "</script>")
    List<ZyUnitDto> selectZyUnitList(ZyUnit zyUnit);
}
