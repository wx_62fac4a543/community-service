package com.zy.community.community.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zy.community.community.domain.ZyBuilding;
import com.zy.community.community.domain.dto.KnDto;
import com.zy.community.community.domain.dto.ZyBuildingDto;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 楼栋 Mapper接口
 *
 * @author ruoyi
 * @date 2020-12-11
 */
public interface ZyBuildingMapper extends BaseMapper<ZyBuilding> {

    /**
     * 查询列表
     * @param zyBuilding
     * @return
     */
    @Select("<script>" +
            "SELECT " +
            "d.building_id,d.building_name,d.building_code," +
            "d.building_acreage,d.community_id,d.create_by," +
            "d.create_time,d.update_by,d.update_time," +
            "d.remark,a.community_name as communityName " +
            "FROM zy_building d " +
            "LEFT JOIN zy_community a on a.community_id = d.community_id" +
            "<where>" +
            "<if test=\"buildingName !=null and buildingName != ''\">" +
            "d.building_name like concat('%',#{buildingName},'%') " +
            "</if>" +
            "and d.community_id = #{communityId}" +
            "${params.dataScope}" +
            "</where>" +
            "</script>")
    List<ZyBuildingDto> selectZyBuildingList(ZyBuilding zyBuilding);

    /**
     * 房间新增所需下拉
     * @param zyBuilding
     * @return
     */
    List<KnDto> queryPullDownRoom(ZyBuilding zyBuilding);
}
