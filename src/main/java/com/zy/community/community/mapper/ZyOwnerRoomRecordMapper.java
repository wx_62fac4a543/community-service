package com.zy.community.community.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zy.community.community.domain.ZyOwnerRoomRecord;
import com.zy.community.community.domain.dto.ZyOwnerRoomRecordDto;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 房屋绑定记录 Mapper接口
 *
 * @author yangdi
 * @date 2020-12-15
 */
public interface ZyOwnerRoomRecordMapper extends BaseMapper<ZyOwnerRoomRecord> {



    /**
     * 查询审核记录
     * @param ownerRoomId
     * @return
     */
    @Select("<script>" +
            "SELECT " +
            "a.owner_room_id,a.community_id,a.building_id," +
            "a.unit_id,a.room_id,a.owner_id," +
            "a.owner_type,a.room_status,a.record_audit_opinion,a.record_audit_type as recordAuditType,a.create_by,a.create_by_id," +
            "a.create_time,a.update_by,a.update_time," +
            "a.remark,b.community_name as communityName,c.building_name as buildingName," +
            "d.unit_name as unitName,e.room_name as roomName,f.owner_real_name as ownerRealName," +
            "CASE a.record_audit_type WHEN 'Web' THEN g.user_name ELSE h.owner_real_name END as createByName " +
            "FROM zy_owner_room_record a " +
            "LEFT JOIN zy_community b on a.community_id = b.community_id " +
            "LEFT JOIN zy_building c on a.building_id = c.building_id " +
            "LEFT JOIN zy_unit d on a.unit_id = d.unit_id " +
            "LEFT JOIN zy_room e on a.room_id = e.room_id " +
            "LEFT JOIN zy_owner f on a.owner_id = f.owner_id " +
            "LEFT JOIN sys_user g on g.user_id = a.create_by_id " +
            "LEFT JOIN zy_owner h on h.owner_id = a.create_by_id " +
            "<where>" +
            "and a.owner_room_id = #{ownerRoomId}" +
            "</where>" +
            "</script>")
    List<ZyOwnerRoomRecordDto> queryOwnerRoomRecordByOwnerRoomId(Long ownerRoomId);


    @Select("select record_audit_opinion from zy_owner_room_record where owner_room_id=#{ownerRoomId} order by create_time desc limit 1")
    String findLastedOpinion(@Param("ownerRoomId") Long ownerRoomId);
}
