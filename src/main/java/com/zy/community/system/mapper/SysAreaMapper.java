package com.zy.community.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zy.community.system.domain.SysArea;

import java.util.List;

/**
 * @author yangdi
 */
public interface SysAreaMapper{

    List<SysArea> findAll();
}
